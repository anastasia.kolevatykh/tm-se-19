package ru.kolevatykh.spring.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.*;
import ru.kolevatykh.spring.endpoint.*;
import ru.kolevatykh.spring.service.*;
import ru.kolevatykh.spring.constant.WSDLConst;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {

    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private ISessionService sessionService;

    @Autowired
    @NotNull private IDomainService domainService;

    @Autowired
    @NotNull private IUserService userService;

    @Autowired
    @NotNull private IProjectService projectService;

    @Autowired
    @NotNull private ITaskService taskService;

    @Autowired
    @NotNull private ITaskEndpoint taskEndpoint;

    @Autowired
    @NotNull private IProjectEndpoint projectEndpoint;

    @Autowired
    @NotNull private IDomainEndpoint domainEndpoint;

    @Autowired
    @NotNull private IUserEndpoint userEndpoint;

    @Autowired
    @NotNull private ITokenEndpoint tokenEndpoint;

    public Bootstrap() {
    }

    public void init() throws Exception {
        Endpoint.publish(WSDLConst.TOKEN_URL, tokenEndpoint);
        Endpoint.publish(WSDLConst.DOMAIN_URL, domainEndpoint);
        Endpoint.publish(WSDLConst.USER_URL, userEndpoint);
        Endpoint.publish(WSDLConst.PROJECT_URL, projectEndpoint);
        Endpoint.publish(WSDLConst.TASK_URL, taskEndpoint);

        System.out.println(WSDLConst.TOKEN_URL);
        System.out.println(WSDLConst.DOMAIN_URL);
        System.out.println(WSDLConst.USER_URL);
        System.out.println(WSDLConst.PROJECT_URL);
        System.out.println(WSDLConst.TASK_URL);

        if (userService.findOneByLogin("admin") == null)
            domainService.loadJacksonJson();
    }
}
