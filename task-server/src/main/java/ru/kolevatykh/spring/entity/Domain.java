package ru.kolevatykh.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.dto.ProjectDTO;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.dto.UserDTO;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "domain")
@XmlRootElement
public final class Domain implements Serializable {

    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    @NotNull
    private List<UserDTO> users;

    @XmlElementWrapper(name = "projects")
    @XmlElement(name = "project")
    @NotNull
    private List<ProjectDTO> projects;

    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    @NotNull
    private List<TaskDTO> tasks;

    public Domain() {
    }

    @NotNull
    public List<UserDTO> getUsers() {
        return this.users;
    }

    @NotNull
    public List<ProjectDTO> getProjects() {
        return this.projects;
    }

    @NotNull
    public List<TaskDTO> getTasks() {
        return this.tasks;
    }

    public void setUsers(@NotNull final List<UserDTO> users) {
        this.users = users;
    }

    public void setProjects(@NotNull final  List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }
}
