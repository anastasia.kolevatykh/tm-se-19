package ru.kolevatykh.spring.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.enumerate.RoleType;

import javax.persistence.*;

@Entity
@Table(name = "app_session")
public final class Session extends AbstractEntity implements Cloneable {

    @Basic
    @Nullable
    private String signature;

    @Basic(optional = false)
    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private RoleType roleType;

    public Session() {
    }

    @Nullable
    public String getSignature() {
        return this.signature;
    }

    @NotNull
    public Long getTimestamp() {
        return this.timestamp;
    }

    @NotNull
    public User getUser() {
        return this.user;
    }

    @NotNull
    public RoleType getRoleType() {
        return this.roleType;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    public void setTimestamp(@NotNull final Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUser(@NotNull final User user) {
        this.user = user;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }
}
