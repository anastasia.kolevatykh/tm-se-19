package ru.kolevatykh.spring.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.enumerate.RoleType;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "app_user")
public final class User extends AbstractEntity {

    @Basic(optional = false)
    @Column(unique = true)
    @NotNull
    private String login;

    @Basic(optional = false)
    @NotNull
    private String passwordHash;

    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private RoleType roleType;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private String phone;

    @Basic(optional = false)
    private Boolean locked = false;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Session> sessions;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Project> projects;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Task> tasks;

    public User(@NotNull final String login,
                @NotNull final String password,
                @NotNull final RoleType roleType) {
        super();
        this.login = login;
        this.passwordHash = password;
        this.roleType = roleType;
    }

    public User() {
    }

    @NotNull
    public String getLogin() {
        return this.login;
    }

    @NotNull
    public String getPasswordHash() {
        return this.passwordHash;
    }

    @NotNull
    public RoleType getRoleType() {
        return this.roleType;
    }

    @Nullable
    public String getEmail() {
        return this.email;
    }

    @Nullable
    public String getFirstName() {
        return this.firstName;
    }

    @Nullable
    public String getLastName() {
        return this.lastName;
    }

    @Nullable
    public String getMiddleName() {
        return this.middleName;
    }

    @Nullable
    public String getPhone() {
        return this.phone;
    }

    @NotNull
    public Boolean getLocked() {
        return this.locked;
    }

    @Nullable
    public List<Session> getSessions() {
        return this.sessions;
    }

    @Nullable
    public List<Project> getProjects() {
        return this.projects;
    }

    @Nullable
    public List<Task> getTasks() {
        return this.tasks;
    }

    public void setLogin(@NotNull final String login) {
        this.login = login;
    }

    public void setPasswordHash(@NotNull final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

    public void setFirstName(@Nullable final String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@Nullable final String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(@Nullable final String middleName) {
        this.middleName = middleName;
    }

    public void setPhone(@Nullable final String phone) {
        this.phone = phone;
    }

    public void setLocked(@NotNull final Boolean locked) {
        this.locked = locked;
    }

    public void setSessions(@Nullable final List<Session> sessions) {
        this.sessions = sessions;
    }

    public void setProjects(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }
}
