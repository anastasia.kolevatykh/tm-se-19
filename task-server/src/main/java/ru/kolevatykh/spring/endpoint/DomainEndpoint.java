package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.IDomainService;
import ru.kolevatykh.spring.entity.Token;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.exception.CommandAdminOnlyException;
import ru.kolevatykh.spring.service.TokenService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.kolevatykh.spring.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private IDomainService domainService;

    public DomainEndpoint() {
    }

    @Override
    @WebMethod
    public void serialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.serialize();
    }

    @Override
    @WebMethod
    public void deserialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.deserialize();
    }

    @Override
    @WebMethod
    public void saveJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.saveJacksonJson();
    }

    @Override
    @WebMethod
    public void loadJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.loadJacksonJson();
    }

    @Override
    @WebMethod
    public void saveJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.saveJacksonXml();
    }

    @Override
    @WebMethod
    public void loadJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.loadJacksonXml();
    }

    @Override
    @WebMethod
    public void marshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.marshalJaxbJson();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.unmarshalJaxbJson();
    }

    @Override
    @WebMethod
    public void marshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.marshalJaxbXml();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        domainService.unmarshalJaxbXml();
    }
}
