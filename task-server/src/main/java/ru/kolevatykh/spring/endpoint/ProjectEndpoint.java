package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.IProjectService;
import ru.kolevatykh.spring.entity.Token;
import ru.kolevatykh.spring.service.TokenService;
import ru.kolevatykh.spring.util.DateFormatterUtil;
import ru.kolevatykh.spring.dto.ProjectDTO;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.enumerate.StatusType;
import ru.kolevatykh.spring.exception.SessionNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.kolevatykh.spring.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private IProjectService projectService;
    
    public ProjectEndpoint() {
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllByUserId(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final ProjectDTO findProjectById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return projectService.getProjectDTO(projectService.findOneById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return projectService.getListProjectDTO(projectService.findOneByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setUserId(sessionDTO.getUserId());
        projectDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        projectDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        projectService.persist(projectService.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final ProjectDTO projectDTO =
                projectService.getProjectDTO(projectService.findOneById(sessionDTO.getUserId(), id));
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setUserId(sessionDTO.getUserId());
        projectDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        projectDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        projectService.merge(projectService.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProjectStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final ProjectDTO projectDTO =
                projectService.getProjectDTO(projectService.findOneById(sessionDTO.getUserId(), id));
        projectDTO.setStatusType(StatusType.valueOf(status));
        projectService.merge(projectService.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        projectService.remove(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws SessionNotFoundException, Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        projectService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByCreateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllSortedByCreateDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllSortedByStartDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllSortedByFinishDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {

        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllSortedByStatus(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                projectService.getListProjectDTO(projectService.findAllBySearch(sessionDTO.getUserId(), search));
        return projects;
    }
}
