package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    List<UserDTO> findAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findUserById(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "loginNew") @NotNull String loginNew,
            @WebParam(name = "password") @NotNull String password,
            @WebParam(name = "role") @Nullable String role
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;
}
