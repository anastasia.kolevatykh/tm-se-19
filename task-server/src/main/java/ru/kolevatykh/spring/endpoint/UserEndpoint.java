package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.ISessionService;
import ru.kolevatykh.spring.api.service.IUserService;
import ru.kolevatykh.spring.entity.Token;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.exception.CommandAdminOnlyException;
import ru.kolevatykh.spring.service.TokenService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.kolevatykh.spring.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private ISessionService sessionService;
    
    @Autowired
    @NotNull private IUserService userService;

    public UserEndpoint() {
    }

    @Override
    @Nullable
    @WebMethod
    public final List<UserDTO> findAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @NotNull final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        return userService.getListUserDTO(userService.findAll());
    }

    @Override
    @Nullable
    @WebMethod
    public final UserDTO findUserById(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @NotNull final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return userService.getUserDTO(userService.findOneById(sessionDTO.getUserId()));
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "loginNew") @NotNull final String loginNew,
            @WebParam(name = "password") @NotNull final String password,
            @WebParam(name = "role") @Nullable final String role
    ) throws Exception {
        @NotNull final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) throw new Exception("[The login is not found.]");
        user.setLogin(loginNew);
        user.setPasswordHash(password);
        user.setRoleType(RoleType.valueOf(role));
        userService.merge(user);
        sessionService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @NotNull final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final String userId = sessionDTO.getUserId();
        userService.remove(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @NotNull final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        userService.removeAll();
    }
}
