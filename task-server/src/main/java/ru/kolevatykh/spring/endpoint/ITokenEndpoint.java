package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.exception.AuthenticationException;
import ru.kolevatykh.spring.exception.EmptyInputException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITokenEndpoint {

    @WebMethod
    void closeTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @WebMethod
    void closeAllTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<SessionDTO> getListTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDTO getUserByToken(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    String openTokenSessionAuth(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws EmptyInputException, AuthenticationException, Exception;

    @Nullable
    @WebMethod
    String openTokenSessionReg(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws EmptyInputException, AuthenticationException, Exception;
}
