package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.ITaskService;
import ru.kolevatykh.spring.entity.Task;
import ru.kolevatykh.spring.entity.Token;
import ru.kolevatykh.spring.service.TokenService;
import ru.kolevatykh.spring.util.DateFormatterUtil;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.enumerate.StatusType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.kolevatykh.spring.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private ITaskService taskService;

    public TaskEndpoint() {
    }

    @Nullable
    @WebMethod
    public final List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllByUserId(sessionDTO.getUserId()));
        return tasks;
    }

    @Nullable
    @WebMethod
    public final TaskDTO findTaskById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return taskService.getTaskDTO(taskService.findOneById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTaskByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return taskService.getListTaskDTO(taskService.findOneByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setProjectId(projectId);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setUserId(sessionDTO.getUserId());
        taskDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        taskDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        taskService.persist(taskService.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                taskService.getTaskDTO(taskService.findOneById(sessionDTO.getUserId(), id));
        taskDTO.setProjectId(projectId);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setUserId(sessionDTO.getUserId());
        taskDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        taskDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        taskService.merge(taskService.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void mergeTaskStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                taskService.getTaskDTO(taskService.findOneById(sessionDTO.getUserId(), id));
        taskDTO.setStatusType(StatusType.valueOf(status));
        taskService.merge(taskService.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        taskService.remove(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        taskService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByCteateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllSortedByCreateDate(sessionDTO.getUserId()));
        return tasks;
    }
    
    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllSortedByStartDate(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllSortedByFinishDate(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllSortedByStatus(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findAllBySearch(sessionDTO.getUserId(), search));
        return tasks;
    }

    @Override
    @WebMethod
    public void removeTasksWithProjectId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        taskService.removeTasksWithProjectId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectTasks(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        taskService.removeProjectTasks(sessionDTO.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksByProjectId(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findTasksByProjectId(sessionDTO.getUserId(), projectId));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksWithoutProject(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                taskService.getListTaskDTO(taskService.findTasksWithoutProject(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @WebMethod
    public void assignToProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                taskService.getTaskDTO(taskService.findOneById(sessionDTO.getUserId(), id));
        taskDTO.setProjectId(projectId);
        taskService.merge(taskService.getTaskEntity(taskDTO));
    }
}
