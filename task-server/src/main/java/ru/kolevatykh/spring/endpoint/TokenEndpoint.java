package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.api.service.ISessionService;
import ru.kolevatykh.spring.api.service.IUserService;
import ru.kolevatykh.spring.entity.Session;
import ru.kolevatykh.spring.entity.Token;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.exception.AuthenticationException;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.InvalidPasswordException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.service.SessionService;
import ru.kolevatykh.spring.service.TokenService;
import ru.kolevatykh.spring.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.kolevatykh.spring.endpoint.ITokenEndpoint")
public class TokenEndpoint extends AbstractEndpoint implements ITokenEndpoint {
    
    @Autowired
    @NotNull private TokenService tokenService;

    @Autowired
    @NotNull private ISessionService sessionService;

    @Autowired
    @NotNull private IUserService userService;
    
    public TokenEndpoint() {
    }

    @Override
    @Nullable
    @WebMethod
    public String openTokenSessionAuth(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws Exception, AuthenticationException, UserNotFoundException {
        try {
            if (login == null || login.isEmpty()) throw new EmptyInputException("Login");
            if (password == null || password.isEmpty()) throw new EmptyInputException("Password");
            @Nullable final String tokenString = tokenService.openAuth(login, password);
            return tokenString;
        } catch (EmptyInputException | InvalidPasswordException e) {
            throw new AuthenticationException();
        }
    }

    @Override
    @Nullable
    @WebMethod
    public String openTokenSessionReg(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws Exception, AuthenticationException, UserNotFoundException {
        try {
            if (login == null || login.isEmpty()) throw new EmptyInputException("Login");
            if (password == null || password.isEmpty()) throw new EmptyInputException("Password");
            @Nullable final String tokenString = tokenService.openReg(login, password);
            return tokenString;
        } catch (EmptyInputException | InvalidPasswordException e) {
            throw new AuthenticationException();
        }
    }

    @Override
    @Nullable
    @WebMethod
    public List<SessionDTO> getListTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        tokenService.validate(tokenString);
        return sessionService.getListSessionDTO(tokenService.getListSession());
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO getUserByToken(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        return userService.getUserDTO(tokenService.getUser(token));
    }

    @Override
    @WebMethod
    public void closeTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        @Nullable final Token token = tokenService.validate(tokenString);
        tokenService.closeSession(token);
    }

    @Override
    @WebMethod
    public void closeAllTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        tokenService.validate(tokenString);
        tokenService.closeAllSession();
    }
}
