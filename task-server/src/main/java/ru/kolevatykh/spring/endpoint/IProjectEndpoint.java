package ru.kolevatykh.spring.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeProjectStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String status
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectsSortedByCreateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectsSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectsSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectsSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectsBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable String search
    ) throws Exception;
}
