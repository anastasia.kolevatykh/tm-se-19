package ru.kolevatykh.spring.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.util.ConfigUtil;
import ru.kolevatykh.spring.util.SignatureUtil;

import java.io.IOException;

public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    private String signature;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @NotNull
    private String userId;

    @NotNull
    private RoleType roleType;

    public SessionDTO() {
    }

    public static String generateSignature(@NotNull final SessionDTO session)
            throws IOException, CloneNotSupportedException {
        @NotNull final SessionDTO tempSession = (SessionDTO) session.clone();
        tempSession.setSignature(null);
        return SignatureUtil.sign(tempSession, ConfigUtil.getSalt(), ConfigUtil.getCycle());
    }

    @Nullable
    public String getSignature() {
        return this.signature;
    }

    @NotNull
    public Long getTimestamp() {
        return this.timestamp;
    }

    @NotNull
    public String getUserId() {
        return this.userId;
    }

    @NotNull
    public RoleType getRoleType() {
        return this.roleType;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    public void setTimestamp(@NotNull final Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUserId(@NotNull final String userId) {
        this.userId = userId;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }
}
