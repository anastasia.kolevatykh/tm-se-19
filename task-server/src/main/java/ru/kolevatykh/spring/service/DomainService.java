package ru.kolevatykh.spring.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.api.service.IDomainService;
import ru.kolevatykh.spring.api.service.IProjectService;
import ru.kolevatykh.spring.api.service.ITaskService;
import ru.kolevatykh.spring.api.service.IUserService;
import ru.kolevatykh.spring.entity.Domain;
import ru.kolevatykh.spring.dto.ProjectDTO;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.dto.UserDTO;

import javax.transaction.Transactional;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class DomainService implements IDomainService {

    @Autowired
    @NotNull private IUserService userService;

    @Autowired
    @NotNull private IProjectService projectService;

    @Autowired
    @NotNull private ITaskService taskService;

    public DomainService() {
    }

    @Nullable
    private Domain findAll() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(userService.getListUserDTO(userService.findAll()));
        domain.setProjects(projectService.getListProjectDTO(projectService.findAll()));
        domain.setTasks(taskService.getListTaskDTO(taskService.findAll()));
        return domain;
    }

    private void loadAll(@NotNull final Domain domain) throws Exception {
        for (@NotNull final UserDTO userDTO : domain.getUsers())
            userService.persist(userService.getUserEntity(userDTO));
        for (@NotNull final ProjectDTO projectDTO : domain.getProjects())
            projectService.persist(projectService.getProjectEntity(projectDTO));
        for (@NotNull final TaskDTO taskDTO : domain.getTasks())
            taskService.persist(taskService.getTaskEntity(taskDTO));
    }

    @Override
    public void serialize() throws Exception {
        @Nullable final Domain domain = findAll();
        @NotNull final FileOutputStream file = new FileOutputStream("task-server/src/main/resources/file/domain.out");
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        file.close();
    }

    @Override
    public void deserialize() throws Exception {
        @NotNull final InputStream is = DomainService.class.getResourceAsStream("/file/domain.out");
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(is);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        is.close();
        loadAll(domain);
    }

    @Override
    public void saveJacksonJson() throws Exception {
        @Nullable final Domain domain = findAll();
        @NotNull final FileOutputStream file = new FileOutputStream("task-server/src/main/resources/file/jacksonDomain.json");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
        file.close();
    }

    @Override
    public void loadJacksonJson() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final InputStream is = DomainService.class.getResourceAsStream("/file/jacksonDomain.json");
        @NotNull final Domain domain = objectMapper.readValue(is, Domain.class);
        loadAll(domain);
        is.close();
    }

    @Override
    public void saveJacksonXml() throws Exception {
        @Nullable final Domain domain = findAll();
        @NotNull final FileOutputStream file = new FileOutputStream("task-server/src/main/resources/file/jacksonDomain.xml");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
        file.close();
    }

    @Override
    public void loadJacksonXml() throws Exception {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final InputStream is = DomainService.class.getResourceAsStream("/file/jacksonDomain.xml");
        @NotNull final Domain domain = xmlMapper.readValue(is, Domain.class);
        loadAll(domain);
        is.close();
    }

    @Override
    public void marshalJaxbJson() throws Exception {
        @Nullable final Domain domain = findAll();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final FileOutputStream file = new FileOutputStream("task-server/src/main/resources/file/jaxbDomain.json");
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
        marshaller.marshal(domain, file);
        file.close();
    }

    @Override
    public void unmarshalJaxbJson() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, false);
        @NotNull final InputStream is = DomainService.class.getResourceAsStream("/file/jaxbDomain.json");
        @NotNull final StreamSource source = new StreamSource(is);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(source);
        loadAll(domain);
        is.close();
    }

    @Override
    public void marshalJaxbXml() throws Exception {
        @Nullable final Domain domain = findAll();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final FileOutputStream file = new FileOutputStream("task-server/src/main/resources/file/jaxbDomain.xml");
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
        file.close();
    }

    @Override
    public void unmarshalJaxbXml() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final InputStream is = DomainService.class.getResourceAsStream("/file/jaxbDomain.xml");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(is);
        loadAll(domain);
        is.close();
    }
}
