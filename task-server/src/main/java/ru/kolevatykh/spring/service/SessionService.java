package ru.kolevatykh.spring.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.api.repository.ISessionRepository;
import ru.kolevatykh.spring.api.service.ISessionService;
import ru.kolevatykh.spring.api.service.IUserService;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.entity.Session;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.exception.*;
import ru.kolevatykh.spring.util.ConfigUtil;
import ru.kolevatykh.spring.util.SignatureUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Autowired
    @NotNull private IUserService userService;

    @Autowired
    @NotNull private ISessionRepository sessionRepository;

    public SessionService() {
    }

    @NotNull
    public Session getSessionEntity(@Nullable final SessionDTO sessionDTO)
            throws Exception, SessionNotFoundException, UserNotFoundException {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        if (userService.findOneById(sessionDTO.getUserId()) != null)
            session.setUser(userService.findOneById(sessionDTO.getUserId()));
        session.setRoleType(sessionDTO.getRoleType());
        return session;
    }

    @NotNull
    public SessionDTO getSessionDTO(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setRoleType(session.getRoleType());
        return sessionDTO;
    }

    @NotNull
    public List<SessionDTO> getListSessionDTO(@Nullable final List<Session> sessions) throws SessionNotFoundException {
        if (sessions == null) throw new SessionNotFoundException();
        @NotNull final List<SessionDTO> listSessionDTO = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            listSessionDTO.add(getSessionDTO(session));
        }
        return listSessionDTO;
    }

    @Nullable
    @Override
    public User getUser(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        return user;
    }

    @NotNull
    @Override
    public SessionDTO openAuth(@NotNull final String login, @NotNull final String password)
            throws Exception, IOException, UserNotFoundException, InvalidPasswordException, SessionNotFoundException {
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(password)) throw new InvalidPasswordException();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRoleType(user.getRoleType());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO, ConfigUtil.getSalt(), ConfigUtil.getCycle()));
        persist(getSessionEntity(sessionDTO));
        return sessionDTO;
    }

    @NotNull
    @Override
    public SessionDTO openReg(@NotNull final String login, @NotNull final String password)
            throws Exception, IOException, DuplicateUserException, UserNotFoundException, SessionNotFoundException {
        if (userService.findOneByLogin(login) != null) throw new DuplicateUserException();
        @Nullable User user = new User(login, password, RoleType.USER);
        userService.persist(user);
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRoleType(user.getRoleType());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO, ConfigUtil.getSalt(), ConfigUtil.getCycle()));
        persist(getSessionEntity(sessionDTO));
        return sessionDTO;
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws IOException, CloneNotSupportedException,
            SessionNotFoundException, SessionExpiredException, SignatureCorruptException {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @NotNull final long timeOut = 5 * 60 * 1000L;
        if (System.currentTimeMillis() - sessionDTO.getTimestamp() > timeOut) {
            remove(sessionDTO.getId());
            throw new SessionExpiredException();
        }
        @NotNull final String signature = SessionDTO.generateSignature(sessionDTO);
        if (!signature.equals(sessionDTO.getSignature()))
            throw new SignatureCorruptException("Session");
        if (findOneById(sessionDTO.getId()) == null)
            throw new SessionNotFoundException();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @NotNull
    public List<Session> findAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return sessionRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws SessionNotFoundException {
        if (id == null || id.isEmpty()) throw new SessionNotFoundException();
        return sessionRepository.getOne(id);
    }

    @Override
    void persist(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        sessionRepository.save(session);
    }

    @Override
    void merge(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        sessionRepository.save(session);
    }

    @Override
    public void remove(@Nullable final String id) throws SessionNotFoundException {
        if (id == null || id.isEmpty()) throw new SessionNotFoundException();
        @NotNull final Session session = sessionRepository.getOne(id);
        sessionRepository.delete(session);
    }

    @Override
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final Session session : findAllByUserId(userId))
            sessionRepository.delete(session);
    }
}
