package ru.kolevatykh.spring.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.api.repository.IProjectRepository;
import ru.kolevatykh.spring.api.repository.IUserRepository;
import ru.kolevatykh.spring.api.service.IProjectService;
import ru.kolevatykh.spring.api.service.ISessionService;
import ru.kolevatykh.spring.api.service.ITaskService;
import ru.kolevatykh.spring.api.service.IUserService;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.exception.UserNotFoundException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User> implements IUserService {

    @Autowired
    @NotNull
    private IUserRepository userRepository;

    @Autowired
    @NotNull
    private ISessionService sessionService;

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Autowired
    @NotNull
    private ITaskService taskService;

    public UserService() {
    }

    @NotNull
    public User getUserEntity(@Nullable final UserDTO userDTO) throws Exception {
        if (userDTO == null) throw new UserNotFoundException();
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRoleType(userDTO.getRoleType());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setPhone(userDTO.getPhone());
        user.setLocked(userDTO.getLocked());
        if (sessionService.findAllByUserId(userDTO.getId()) != null)
            user.setSessions(sessionService.findAllByUserId(userDTO.getId()));
        user.setProjects(projectService.findAllByUserId(userDTO.getId()));
        user.setTasks(taskService.findAllByUserId(userDTO.getId()));
        return user;
    }

    @NotNull
    public UserDTO getUserDTO(@Nullable final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRoleType(user.getRoleType());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setPhone(user.getPhone());
        userDTO.setLocked(user.getLocked());
        return userDTO;
    }

    @NotNull
    public List<UserDTO> getListUserDTO(@Nullable final List<User> users) throws UserNotFoundException {
        if (users == null) throw new UserNotFoundException();
        @NotNull final List<UserDTO> listUserDTO = new ArrayList<>();
        for (@NotNull final User user : users) {
            listUserDTO.add(getUserDTO(user));
        }
        return listUserDTO;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws Exception, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        return userRepository.getOne(id);
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) throws Exception, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new UserNotFoundException();
        return userRepository.findOneByLogin(login);
    }

    @Override
    public void persist(@Nullable final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        userRepository.save(user);
    }

    @Override
    public void remove(@Nullable String id) throws Exception, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        @NotNull final User user = userRepository.getOne(id);
        userRepository.delete(user);
    }

    @Override
    public void removeAll() {
        userRepository.deleteAll();
    }
}
