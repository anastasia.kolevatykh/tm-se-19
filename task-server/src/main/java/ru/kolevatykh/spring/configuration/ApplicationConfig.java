package ru.kolevatykh.spring.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("ru.kolevatykh.spring")
@EnableJpaRepositories("ru.kolevatykh.spring.api.repository")
@Import(DataSourceConfig.class)
public class ApplicationConfig {

}
