package ru.kolevatykh.spring;

import org.apache.log4j.PropertyConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kolevatykh.spring.bootstrap.Bootstrap;
import ru.kolevatykh.spring.configuration.ApplicationConfig;

import java.io.InputStream;

public final class ApplicationServer {
    public static void main(String[] args) throws Exception {
        @NotNull final InputStream is = ApplicationServer.class.getResourceAsStream("/META-INF/log4j.properties");
        PropertyConfigurator.configure(is);
        is.close();
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}
