package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.api.IService;
import ru.kolevatykh.spring.dto.SessionDTO;
import ru.kolevatykh.spring.entity.Session;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.exception.SessionNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @NotNull Session getSessionEntity(@Nullable final SessionDTO sessionDTO) throws Exception;

    @NotNull SessionDTO getSessionDTO(@Nullable final Session session) throws SessionNotFoundException;

    @NotNull List<SessionDTO> getListSessionDTO(@Nullable final List<Session> sessions) throws SessionNotFoundException;

    @NotNull List<Session> findAllByUserId(@Nullable final String userId) throws UserNotFoundException;

    @Nullable User getUser(@Nullable SessionDTO session) throws Exception;

    @NotNull SessionDTO openAuth(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull SessionDTO openReg(@NotNull String login, @NotNull String password) throws Exception;

    void validate(@Nullable SessionDTO sessionDTO) throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;
}
