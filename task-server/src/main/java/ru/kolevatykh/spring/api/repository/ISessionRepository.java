package ru.kolevatykh.spring.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kolevatykh.spring.entity.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {

    @Query(value = "SELECT s FROM Session s WHERE s.user.id = :userId")
    @NotNull List<Session> findAllByUserId(@Param("userId") @Nullable final String userId);
}
