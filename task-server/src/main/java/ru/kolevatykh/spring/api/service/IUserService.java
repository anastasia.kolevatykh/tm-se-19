package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.api.IService;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.exception.UserNotFoundException;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull User getUserEntity(@Nullable final UserDTO userDTO) throws Exception;

    @NotNull UserDTO getUserDTO(@Nullable final User user) throws UserNotFoundException;

    @NotNull List<UserDTO> getListUserDTO(@Nullable final List<User> users) throws UserNotFoundException;

    @Nullable List<User> findAll() throws Exception;

    @Nullable User findOneById(@Nullable String id) throws Exception;

    @Nullable User findOneByLogin(@Nullable String login) throws UserNotFoundException, Exception;

    void persist(@Nullable User user) throws UserNotFoundException;

    void merge(@Nullable User user) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll() throws Exception;
}
