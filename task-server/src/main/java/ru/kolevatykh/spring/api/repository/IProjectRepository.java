package ru.kolevatykh.spring.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kolevatykh.spring.entity.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId")
    @NotNull List<Project> findAllByUserId(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id")
    @Nullable Project findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId AND p.name = :name")
    @NotNull List<Project> findOneByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.createDate ASC")
    @NotNull List<Project> findAllSortedByCreateDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.startDate ASC")
    @NotNull List<Project> findAllSortedByStartDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.finishDate ASC")
    @NotNull List<Project> findAllSortedByFinishDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT * FROM app_project WHERE user_id = :userId ORDER BY FIELD(statusType, 'PLANNED', 'INPROCESS', 'READY')", nativeQuery = true)
    @NotNull List<Project> findAllSortedByStatus(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT p FROM Project p WHERE p.user.id = :userId AND (p.name LIKE :search OR p.description LIKE :search)")
    @NotNull List<Project> findAllBySearch(@Param("userId") @NotNull String userId, @Param("search") @NotNull String search) throws Exception;
}
