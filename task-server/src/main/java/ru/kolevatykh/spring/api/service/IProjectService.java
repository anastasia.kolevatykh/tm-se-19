package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.api.IProjectTaskService;
import ru.kolevatykh.spring.dto.ProjectDTO;
import ru.kolevatykh.spring.entity.Project;

import java.util.List;

public interface IProjectService extends IProjectTaskService<Project> {

    @NotNull Project getProjectEntity(@Nullable final ProjectDTO projectDTO) throws Exception;

    @NotNull ProjectDTO getProjectDTO(@Nullable final Project project) throws Exception;

    @NotNull List<ProjectDTO> getListProjectDTO(@Nullable final List<Project> projects) throws Exception;

    @NotNull List<Project> findAll() throws Exception;

    @NotNull List<Project> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<Project> findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable Project project) throws Exception;

    void merge(@Nullable Project project) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;

    @NotNull List<Project> findAllSortedByCreateDate(@Nullable String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStartDate(@Nullable String userId) throws Exception;

    @NotNull List<Project> findAllSortedByFinishDate(@Nullable String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStatus(@Nullable String userId) throws Exception;

    @NotNull List<Project> findAllBySearch(@Nullable String userId, @Nullable String search) throws Exception;
}