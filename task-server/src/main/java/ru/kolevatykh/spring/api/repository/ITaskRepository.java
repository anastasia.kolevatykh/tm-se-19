package ru.kolevatykh.spring.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kolevatykh.spring.entity.Task;
import ru.kolevatykh.spring.exception.UserNotFoundException;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId")
    @NotNull List<Task> findAllByUserId(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id")
    @Nullable Task findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.name = :name")
    @Nullable List<Task> findOneByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.project IS NOT NULL")
    @NotNull List<Task> findTasksWithProjectId(@Param("userId") @Nullable final String userId) throws UserNotFoundException;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
    @NotNull List<Task> findTasksByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id IS NULL")
    @NotNull List<Task> findTasksWithoutProject(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.createDate ASC")
    @NotNull List<Task> findAllSortedByCreateDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.startDate ASC")
    @NotNull List<Task> findAllSortedByStartDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.finishDate ASC")
    @NotNull List<Task> findAllSortedByFinishDate(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT * FROM app_task WHERE user_id = :userId ORDER BY FIELD(statusType, 'PLANNED', 'INPROCESS', 'READY')", nativeQuery = true)
    @NotNull List<Task> findAllSortedByStatus(@Param("userId") @NotNull String userId) throws Exception;

    @Query(value = "SELECT t FROM Task t WHERE t.user.id = :userId AND (t.name LIKE :search OR t.description LIKE :search)")
    @NotNull List<Task> findAllBySearch(@Param("userId") @NotNull String userId, @Param("search") @NotNull String search) throws Exception;
}
