package ru.kolevatykh.spring.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kolevatykh.spring.entity.Project;
import ru.kolevatykh.spring.entity.User;

import java.util.List;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    @Query(value = "SELECT u FROM User u WHERE u.login = :login")
    @Nullable User findOneByLogin(@Param("login") @NotNull String login) throws Exception;
}
