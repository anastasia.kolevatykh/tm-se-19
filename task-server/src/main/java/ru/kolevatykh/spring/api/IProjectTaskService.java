package ru.kolevatykh.spring.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService<T> {

    @NotNull List<T> findAll() throws Exception;

    @NotNull List<T> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable T findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<T> findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable T entity) throws Exception;

    void merge(@Nullable T entity) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;

    @NotNull List<T> findAllSortedByCreateDate(@Nullable String userId) throws Exception;

    @NotNull List<T> findAllSortedByStartDate(@Nullable String userId) throws Exception;

    @NotNull List<T> findAllSortedByFinishDate(@Nullable String userId) throws Exception;

    @NotNull List<T> findAllSortedByStatus(@Nullable String userId) throws Exception;

    @NotNull List<T> findAllBySearch(@Nullable String userId, @Nullable String search) throws Exception;
}
