package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.api.IProjectTaskService;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.entity.Task;
import ru.kolevatykh.spring.exception.UserNotFoundException;

import java.util.List;

public interface ITaskService extends IProjectTaskService<Task> {

    @NotNull Task getTaskEntity(@Nullable final TaskDTO taskDTO) throws Exception;

    @NotNull TaskDTO getTaskDTO(@Nullable final Task task) throws Exception;

    @NotNull List<TaskDTO> getListTaskDTO(@Nullable final List<Task> tasks) throws Exception;

    @NotNull List<Task> findAll() throws Exception;

    @NotNull List<Task> findAllByUserId(@Nullable String userId) throws Exception;

    @NotNull List<Task> findTasksWithProjectId(@Nullable final String userId) throws UserNotFoundException;

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<Task> findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable Task task) throws Exception;

    void merge(@Nullable Task task) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;

    void removeTasksWithProjectId(@Nullable String userId) throws Exception;

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull List<Task> findTasksWithoutProject(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStartDate(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByFinishDate(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStatus(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllBySearch(@Nullable String userId, @Nullable String search) throws Exception;
}
