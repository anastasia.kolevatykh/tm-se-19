package ru.kolevatykh.spring.api.service;

public interface IDomainService {

    void serialize() throws Exception;

    void deserialize() throws Exception;

    void saveJacksonJson() throws Exception;

    void loadJacksonJson() throws Exception;

    void saveJacksonXml() throws Exception;

    void loadJacksonXml() throws Exception;

    void marshalJaxbJson() throws Exception;

    void unmarshalJaxbJson() throws Exception;

    void marshalJaxbXml() throws Exception;

    void unmarshalJaxbXml() throws Exception;
}
