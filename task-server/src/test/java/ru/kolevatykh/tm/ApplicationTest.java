package ru.kolevatykh.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import ru.kolevatykh.spring.entity.Project;
import ru.kolevatykh.spring.entity.Task;
import ru.kolevatykh.spring.entity.User;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.service.ProjectService;
import ru.kolevatykh.spring.service.TaskService;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.PasswordHashUtil;

import java.util.List;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void shouldFindUserByLogin() throws Exception {
        @NotNull final UserService userService = new UserService();
        assertNotNull(userService);
        @Nullable User user = userService.findOneByLogin("demo");
        assertNull(user);
        @NotNull final User test = new User("demo", "pass", RoleType.USER);
        userService.persist(test);
        assertNotNull(test);
        user = userService.findOneByLogin("demo");
        assertNotNull(user);
        userService.remove(user.getId());
        user = userService.findOneByLogin("demo");
        assertNull(user);
    }

    @Test
    public void shouldAssignTaskToProject() throws Exception {
        @NotNull final UserService userService = new UserService();
        @NotNull final User testUser = new User("test", PasswordHashUtil.getPasswordHash("test"), RoleType.ADMIN);
        userService.persist(testUser);
        assertNotNull(testUser);
        @Nullable User user = userService.findOneByLogin("test");
        assertNotNull(user);
        @NotNull final ProjectService projectService = new ProjectService();
        @NotNull final TaskService taskService = new TaskService();
        @NotNull final Project project = new Project("demo project", "desc project 1", null, null);
        project.setUser(user);
        projectService.persist(project);
        @NotNull final Task task = new Task("demo task", "desc task 1", null, null);
        task.setUser(user);
        taskService.persist(task);
        assertFalse(taskService.findOneByName(task.getUser().getId(), "demo task").isEmpty());
        assertFalse(projectService.findOneByName(project.getUser().getId(), "demo project").isEmpty());
        task.setProject(project);
        taskService.merge(task);
        assertNotNull(task);
        @Nullable final List<Task> testTask = taskService.findOneByName(task.getUser().getId(), "demo task");
        assertNotNull(testTask);
        assertNotNull(testTask.get(0).getProject());
        taskService.remove(user.getId(), task.getId());
        assertFalse(taskService.findOneByName(task.getUser().getId(), "demo task").isEmpty());
        projectService.remove(user.getId(), project.getId());
        assertFalse(projectService.findOneByName(user.getId(), "demo project").isEmpty());
        userService.remove(user.getId());
        assertNull(userService.findOneByLogin("test"));
    }
}
