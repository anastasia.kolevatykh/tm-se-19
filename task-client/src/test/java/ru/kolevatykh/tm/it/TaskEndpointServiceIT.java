package ru.kolevatykh.tm.it;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.spring.constant.WSDLConst;
import ru.kolevatykh.spring.endpoint.*;
import ru.kolevatykh.spring.util.PasswordHashUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class TaskEndpointServiceIT {

    @NotNull
    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();

    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpointService(new URL(WSDLConst.PROJECT_URL)).getProjectEndpointPort();

    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpointService(new URL(WSDLConst.TASK_URL)).getTaskEndpointPort();

    @Nullable
    private String token;

    TaskEndpointServiceIT() throws MalformedURLException {
    }

    @BeforeEach
    void setUp() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final String login = "testTaskUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTaskUser");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
        projectEndpoint.persistProject(token, "demo project", "", "1.1.2019", "1.1.2020");
        taskEndpoint.persistTask(token, null, "demo task", "", "1.1.2019", "1.1.2020");
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void findTasksWithoutProject() throws Exception_Exception {
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksWithoutProject(token);
        assertFalse(tasks.isEmpty());
    }

    @Test
    void findTaskById() throws Exception_Exception {
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(token, "demo task");
        assertNotNull(taskEndpoint.findTaskById(token, tasks.get(0).getId()));
    }

    @Test
    void findTasksByProjectId() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull final List<TaskDTO> tasksByName = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, projects.get(0).getId(), tasksByName.get(0).getId(), tasksByName.get(0).getName(),
                tasksByName.get(0).getDescription(), "1.1.2019", "1.1.2020");
        @NotNull final List<TaskDTO> tasksByProjectId = taskEndpoint.findTasksByProjectId(token, projects.get(0).getId());
        assertFalse(tasksByProjectId.isEmpty());
    }

    @Test
    void persistTask() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 3", "", "1.1.2019", "1.1.2020");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        for (@NotNull final TaskDTO task : tasks)
            if (task.getName().equals("demo task 3"))
                assertEquals("demo task 3", task.getName());
        assertFalse(tasks.isEmpty());
    }

    @Test
    void mergeTask() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, projects.get(0).getId(), tasks.get(0).getId(), tasks.get(0).getName(),
                tasks.get(0).getDescription(), "1.1.2019", "1.1.2020");
        @NotNull final TaskDTO tempTask = taskEndpoint.findTaskById(token, tasks.get(0).getId());
        assertEquals(projects.get(0).getId(), tempTask.getProjectId());
    }

    @Test
    void removeTasksWithProjectId() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull final List<TaskDTO> tasksByName = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, projects.get(0).getId(), tasksByName.get(0).getId(), tasksByName.get(0).getName(),
                tasksByName.get(0).getDescription(), "1.1.2019", "1.1.2020");
        taskEndpoint.removeTasksWithProjectId(token);
        @NotNull final List<TaskDTO> tasksByProjectId = taskEndpoint.findTasksByProjectId(token, projects.get(0).getId());
        assertTrue(tasksByProjectId.isEmpty());
    }

    @Test
    void findTasksSortedByCteateDate() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2019", "1.1.2020");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksSortedByCteateDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getCreateDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getCreateDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findTasksSortedByStartDate() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2018", "1.1.2021");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksSortedByStartDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getStartDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getStartDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findTasksSortedByFinishDate() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksSortedByFinishDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getFinishDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getFinishDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void assignToProject() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, projects.get(0).getId(), tasks.get(0).getId(), "demo task assigned",
                tasks.get(0).getDescription(), "1.1.2010", "1.1.2020");
        @Nullable final TaskDTO testTask = taskEndpoint.findTaskById(token, tasks.get(0).getId());
        assertNotNull(testTask);
        assertNotNull(testTask.getProjectId());
    }

    @Test
    void findAllTasksByUserId() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 2", "", "12.12.2012", "12.12.2020");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertFalse(tasks.isEmpty());
        assertEquals(2, tasks.size());
    }

    @Test
    void findTaskByName() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 3", "", "12.12.2012", "12.12.2020");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(token, "demo task 3");
        assertFalse(tasks.isEmpty());
    }

    @Test
    void mergeTaskStatus() throws Exception_Exception {
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTaskStatus(token, tasks.get(0).getId(), "READY");
        @NotNull final TaskDTO tempTask = taskEndpoint.findTaskById(token, tasks.get(0).getId());
        assertNotEquals(tasks.get(0).getStatusType().value(), tempTask.getStatusType().value());
    }

    @Test
    void removeProjectTasks() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull final List<TaskDTO> tasksByName = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, projects.get(0).getId(), tasksByName.get(0).getId(), "demo task assigned",
                tasksByName.get(0).getDescription(), "1.1.2010", "1.1.2020");
        taskEndpoint.removeProjectTasks(token, projects.get(0).getId());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        for (@NotNull final TaskDTO taskDTO : tasks)
            if (taskDTO.getProjectId().equals(projects.get(0).getId()))
                assertNotEquals(projects.get(0).getId(), taskDTO.getProjectId());
        assertTrue(tasks.isEmpty());
    }

    @Test
    void findTasksSortedByStatus() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<TaskDTO> tempTasks = taskEndpoint.findTaskByName(token, "demo task 2");
        taskEndpoint.mergeTaskStatus(token, tempTasks.get(0).getId(), "READY");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksSortedByStatus(token);
        @NotNull int prevStatus = 0;
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull int currStatus = task.getStatusType().ordinal();
            assertFalse(prevStatus > currStatus);
            prevStatus = currStatus;
        }
    }

    @Test
    void findTasksBySearch() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "mega task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTasksBySearch(token, "mega");
        for (@NotNull final TaskDTO task : tasks)
            assertTrue(task.getName().contains("mega") || task.getDescription().contains("mega"));
    }

    @Test
    void removeTask() throws Exception_Exception {
        @NotNull final List<TaskDTO> tempTasks = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.removeTask(token, tempTasks.get(0).getId());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertTrue(tasks.isEmpty());
    }

    @Test
    void removeAllTasks() throws Exception_Exception {
        taskEndpoint.persistTask(token, null, "mega task 2", "", "1.1.2009", "1.1.2010");
        taskEndpoint.persistTask(token, null, "mini task 3", "", "1.1.2011", "1.1.2013");
        taskEndpoint.removeAllTasks(token);
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertTrue(tasks.isEmpty());
    }
}