//package ru.kolevatykh.tm.load;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.testng.Assert;
//import org.testng.annotations.*;
//import ru.kolevatykh.tm.constant.WSDLConst;
//import ru.kolevatykh.tm.endpoint.*;
//import ru.kolevatykh.tm.util.PasswordHashUtil;
//
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.UUID;
//
//public class TokenEndpointServiceLoad {
//
//    @NotNull
//    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();
//
//    @NotNull
//    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();
//
//    int testCount = 0;
//
//    public TokenEndpointServiceLoad() throws MalformedURLException {
//    }
//
//    @Test(invocationCount = 1000, threadPoolSize = 100)
//    void getUserByToken() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
//        @NotNull final String login = "testLoad" + UUID.randomUUID();
//        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testLoad");
//        @Nullable String token = tokenEndpoint.openTokenSessionReg(login, pass);
//        System.out.println("Test №" + testCount++);
//        System.out.println("Token: " + token);
//        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
//        Assert.assertNotNull(user);
//        userEndpoint.removeUser(token);
//    }
//}