package ru.kolevatykh.spring.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kolevatykh.spring.endpoint.*;

@Configuration
@ComponentScan("ru.kolevatykh.spring")
public class ApplicationConfig {

    @Bean
    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return new DomainEndpointService().getDomainEndpointPort();
    }

    @Bean
    @NotNull
    public ITokenEndpoint getTokenEndpoint() {
        return new TokenEndpointService().getTokenEndpointPort();
    }
}
