package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JacksonLoadXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-Load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JACKSON XML LOAD]");
        bootstrap.getDomainEndpoint().loadJacksonXml(token);
        System.out.println("[Saving is successful!]");
    }
}
