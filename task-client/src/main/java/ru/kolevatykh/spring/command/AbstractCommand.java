package ru.kolevatykh.spring.command;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.bootstrap.Bootstrap;

public abstract class AbstractCommand {

    @NotNull
    protected Bootstrap bootstrap;

    public AbstractCommand(final Bootstrap bootstrap) {
        this.setBootstrap(bootstrap);
    }

    public AbstractCommand() {
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getShortName();

    @NotNull
    public abstract String getDescription();

    public abstract boolean needAuth();

    public abstract void execute() throws Exception;

    public Bootstrap getBootstrap() {
        return this.bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
