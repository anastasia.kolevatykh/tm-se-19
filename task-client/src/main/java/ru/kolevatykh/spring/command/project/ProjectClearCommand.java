package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.constant.Message;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects with tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        bootstrap.getTaskEndpoint().removeTasksWithProjectId(token);
        bootstrap.getProjectEndpoint().removeAllProjects(token);
        System.out.println("[" + getDescription() + "]\n" + Message.OK);
    }
}
