package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.endpoint.ProjectDTO;
import ru.kolevatykh.spring.wrapper.ProjectWrapper;

import java.util.List;

public final class ProjectListSortedByStartDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list-sorted-by-start-date";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "plssd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects sorted by start date.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        @Nullable final List<ProjectDTO> projectList = bootstrap.getProjectEndpoint().findProjectsSortedByStartDate(token);
        if (projectList == null) {
            throw new Exception("[No projects yet.]");
        }

        @NotNull final StringBuilder projects = new StringBuilder();
        int i = 0;
        for (@NotNull final ProjectDTO projectDTO : projectList) {
            @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper(projectDTO);
            projects.append(++i)
                    .append(". ")
                    .append(projectWrapper.toString())
                    .append(System.lineSeparator());
        }
        @NotNull final String projectString = projects.toString();
        System.out.println(projectString);
    }
}
