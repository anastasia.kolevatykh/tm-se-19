package ru.kolevatykh.spring.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommands()) {
            int length = command.getName().length() + command.getShortName().length() + 3;
            int tabCount = 1;
            if (length <= 10)
                tabCount = 9;
            if (length > 10 & length <= 14)
                tabCount = 8;
            if (length > 14 & length <= 18)
                tabCount = 7;
            if (length > 18 & length <= 22)
                tabCount = 6;
            if (length > 22 & length <= 26)
                tabCount = 5;
            if (length > 26 & length <= 30)
                tabCount = 4;
            if (length > 30 & length <= 34)
                tabCount = 3;
            if (length > 34 & length <= 38)
                tabCount = 2;
            @NotNull final StringBuilder space = new StringBuilder("\t");
            for (int i = 0; i < tabCount; i++)
                space.append("\t");
            System.out.println(command.getName() + " / " + command.getShortName() + ":\t" + space + command.getDescription());
        }
    }
}
