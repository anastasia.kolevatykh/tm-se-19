package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.ProjectDTO;

public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.ID);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final ProjectDTO projectDTO = bootstrap.getProjectEndpoint().findProjectById(token, id);
        if (projectDTO == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        System.out.println(Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.DESCRIPTION);
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.START_DATE);
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.FINISH_DATE);
        @NotNull final String finishDate = ConsoleInputUtil.getConsoleInput();

        bootstrap.getProjectEndpoint().mergeProject(token, id, name, description, startDate, finishDate);
        System.out.println(Message.OK);
    }
}
