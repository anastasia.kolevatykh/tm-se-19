package ru.kolevatykh.spring.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.util.PasswordHashUtil;
import ru.kolevatykh.spring.constant.Message;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ue";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update login or password.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter your login for edit: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The login can't be empty.]");
        }

        System.out.println("Enter new login: ");
        @NotNull final String loginNew = ConsoleInputUtil.getConsoleInput();
        if (loginNew.isEmpty()) {
            throw new Exception("[The login can't be empty.]");
        }

        System.out.println("Enter new password: ");
        @NotNull final String passwordNew = ConsoleInputUtil.getConsoleInput();
        if (passwordNew.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        System.out.println("Enter new role type: ADMIN USER");
        @NotNull final String role = ConsoleInputUtil.getConsoleInput();
        if (role.isEmpty()) {
            throw new Exception("[The role can't be empty.]");
        }

        bootstrap.getUserEndpoint().mergeUser(token, login, loginNew,
                PasswordHashUtil.getPasswordHash(passwordNew), role);
        System.out.println(Message.OK);
    }
}
