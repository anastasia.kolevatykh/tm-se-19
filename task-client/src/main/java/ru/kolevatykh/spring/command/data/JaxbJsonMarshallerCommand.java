package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JaxbJsonMarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "json-marshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "jm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save to json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JSON MARSHALLER]");
        bootstrap.getDomainEndpoint().marshalJaxbJson(token);
        System.out.println("[Saving is successful!]");
    }
}
