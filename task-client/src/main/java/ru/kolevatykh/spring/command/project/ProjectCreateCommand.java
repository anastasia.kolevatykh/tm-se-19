package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();

        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println(Message.DESCRIPTION);
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.START_DATE);
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.FINISH_DATE);
        @NotNull final String finishDate = ConsoleInputUtil.getConsoleInput();

        bootstrap.getProjectEndpoint().persistProject(token, name, description, startDate, finishDate);
        System.out.println(Message.OK);
    }
}
