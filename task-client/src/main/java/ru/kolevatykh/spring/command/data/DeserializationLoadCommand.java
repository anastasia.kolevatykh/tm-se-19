package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class DeserializationLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "l";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[LOAD (deserialization)]");
        bootstrap.getDomainEndpoint().deserialize(token);
        System.out.println("[Loading is successful!]");
    }
}
