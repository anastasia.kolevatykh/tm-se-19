package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JaxbXmlMarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "xml-marshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save all projects and tasks to xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[XML MARSHALLER]");
        bootstrap.getDomainEndpoint().marshalJaxbXml(token);
        System.out.println("[Saving is successful!]");
    }
}
