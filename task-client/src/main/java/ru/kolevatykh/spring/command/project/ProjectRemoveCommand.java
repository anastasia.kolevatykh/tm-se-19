package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.ProjectDTO;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project with tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println(Message.NAME);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final ProjectDTO projectDTO = bootstrap.getProjectEndpoint().findProjectById(token, id);
        if (projectDTO == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        bootstrap.getProjectEndpoint().removeProject(token, id);
        System.out.println("[" + getDescription() + "]\n" + Message.OK);
    }
}
