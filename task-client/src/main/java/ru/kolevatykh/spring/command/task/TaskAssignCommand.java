package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.ProjectDTO;
import ru.kolevatykh.spring.endpoint.TaskDTO;

public final class TaskAssignCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-assign";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ta";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Assign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter task id for assignment:");
        @NotNull final String taskId = ConsoleInputUtil.getConsoleInput();
        if (taskId.isEmpty()) {
            throw new Exception("[The task id can't be empty.]");
        }

        @Nullable final TaskDTO taskDTO = bootstrap.getTaskEndpoint().findTaskById(token, taskId);
        if (taskDTO == null) {
            throw new Exception("[The task id '" + taskId + "' does not exist!]");
        }

        System.out.println("Enter project id to assign task to:");
        @NotNull final String projectId = ConsoleInputUtil.getConsoleInput();
        if (projectId.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final ProjectDTO projectDTO = bootstrap.getProjectEndpoint().findProjectById(token, projectId);
        if (projectDTO == null) {
            throw new Exception("[The project id '" + projectId + "' does not exist!]");
        }

        bootstrap.getTaskEndpoint().assignToProject(token, taskId, projectId);
        System.out.println(Message.OK);
    }
}
