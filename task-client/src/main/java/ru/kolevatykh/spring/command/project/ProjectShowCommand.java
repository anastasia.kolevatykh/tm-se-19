package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.ProjectDTO;
import ru.kolevatykh.spring.endpoint.TaskDTO;
import ru.kolevatykh.spring.wrapper.TaskWrapper;

import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "psh";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final List<ProjectDTO> projects = bootstrap.getProjectEndpoint().findProjectByName(token, name);
        if (projects == null || projects.isEmpty()) {
            throw new Exception("[The project '" + name + "' does not exist!]");
        }

        @NotNull final String id = projects.get(0).getId();
        @Nullable final List<TaskDTO> taskList = bootstrap.getTaskEndpoint().findTasksByProjectId(token, id);
        if (taskList != null) {
            System.out.println("[TASK LIST]");
            @NotNull final StringBuilder tasks = new StringBuilder();
            int i = 0;

            for (@NotNull final TaskDTO taskDTO : taskList) {
                if (id.equals(taskDTO.getProjectId())) {
                    @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
                    tasks
                            .append(++i)
                            .append(". ")
                            .append(taskWrapper.toString())
                            .append(System.lineSeparator());
                }
            }
            @NotNull final String taskString = tasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
