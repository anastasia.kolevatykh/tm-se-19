package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JaxbJsonUnmarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "json-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ju";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load from json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JSON UNMARSHALLER]");
        bootstrap.getDomainEndpoint().unmarshalJaxbJson(token);
        System.out.println("[Loading is successful!]");
    }
}
