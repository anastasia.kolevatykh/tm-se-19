package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JaxbXmlUnmarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "xml-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load from xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[XML UNMARSHALLER]");
        bootstrap.getDomainEndpoint().unmarshalJaxbXml(token);
        System.out.println("[Loading is successful!]");
    }
}
