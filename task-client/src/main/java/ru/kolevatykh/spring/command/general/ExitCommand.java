package ru.kolevatykh.spring.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "e";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTokenEndpoint().closeTokenSession(bootstrap.getToken());
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
