package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class SerializationSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "s";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[SAVE (serialization)]");
        bootstrap.getDomainEndpoint().serialize(token);
        System.out.println("[Saving is successful!]");
    }
}
