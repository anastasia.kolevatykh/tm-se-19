package ru.kolevatykh.spring.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.endpoint.ProjectDTO;
import ru.kolevatykh.spring.endpoint.TaskDTO;
import ru.kolevatykh.spring.endpoint.UserDTO;
import ru.kolevatykh.spring.wrapper.ProjectWrapper;
import ru.kolevatykh.spring.wrapper.TaskWrapper;

import java.util.List;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ush";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects and tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        @Nullable final UserDTO userDTO = bootstrap.getTokenEndpoint().getUserByToken(token);
        if (userDTO == null) {
            throw new Exception("[The user does not exist!]");
        }

        System.out.println("[LOGIN]");
        System.out.println(userDTO.getLogin());
        System.out.println("[ROLE]");
        System.out.println(userDTO.getRoleType());

        @Nullable final List<ProjectDTO> projectList = bootstrap.getProjectEndpoint().findAllProjectsByUserId(token);

        if (projectList != null) {
            System.out.println("[PROJECT LIST]");
            int projectCount = 0;

            for (@NotNull final ProjectDTO projectDTO : projectList) {
                @Nullable final List<TaskDTO> taskList = bootstrap.getTaskEndpoint()
                        .findTasksByProjectId(token, projectDTO.getId());

                @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper(projectDTO);
                System.out.println(++projectCount + ". " + projectWrapper.toString());

                if (taskList != null) {
                    System.out.println("[TASK LIST]");
                    @Nullable final StringBuilder projectTasks = new StringBuilder();
                    int taskCount = 0;

                    for (@NotNull final TaskDTO taskDTO : taskList) {
                        @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
                        projectTasks
                                .append(projectCount)
                                .append(".")
                                .append(++taskCount)
                                .append(". ")
                                .append(taskWrapper.toString())
                                .append("\n");
                    }

                    @NotNull final String taskString = projectTasks.toString();
                    System.out.println(taskString);
                } else {
                    System.out.println("[No tasks yet.]");
                }
            }
        }

        @Nullable final List<TaskDTO> taskNotAssignedList = bootstrap.getTaskEndpoint().findTasksWithoutProject(token);

        if (taskNotAssignedList != null) {
            System.out.println("[NOT ASSIGNED TASKS]");
            @NotNull final StringBuilder taskNotAssigned = new StringBuilder();
            int taskCount = 0;

            for (@NotNull final TaskDTO taskDTO : taskNotAssignedList) {
                @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
                taskNotAssigned
                        .append(++taskCount)
                        .append(". ")
                        .append(taskWrapper.toString())
                        .append(System.lineSeparator());
            }

            @NotNull final String taskString = taskNotAssigned.toString();
            System.out.println(taskString);
        }

        if (projectList == null & taskNotAssignedList == null) {
            throw new Exception("[No projects and tasks yet.]");
        }
    }
}
