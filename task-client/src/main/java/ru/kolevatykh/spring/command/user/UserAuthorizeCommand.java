package ru.kolevatykh.spring.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.util.PasswordHashUtil;
import ru.kolevatykh.spring.constant.Message;

public final class UserAuthorizeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-authorize";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ua";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign in.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String currentToken = bootstrap.getToken();
        if (currentToken != null) {
            throw new Exception("[You are authorized under login: "
                    + bootstrap.getTokenEndpoint().getUserByToken(currentToken).getLogin()
                    + ". Please LOGOUT first, in order to authorize under OTHER account.]");
        }

        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println(Message.LOGIN);
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.PASSWORD);
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();
        if (password.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        @NotNull final String pass = PasswordHashUtil.getPasswordHash(password);
        @NotNull final String token = bootstrap.getTokenEndpoint().openTokenSessionAuth(login, pass);
        bootstrap.setToken(token);
        System.out.println("[OK]");
    }
}
