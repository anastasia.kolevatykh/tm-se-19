package ru.kolevatykh.spring.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.endpoint.UserDTO;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ulo";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Logout from account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter your login to confirm logout: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final String token = bootstrap.getToken();
        @NotNull final UserDTO userDTO = bootstrap.getTokenEndpoint().getUserByToken(token);
        if (!userDTO.getLogin().equals(login)) {
            throw new Exception("[The login '" + login + "' is wrong! Enter correct login.]");
        }

        System.out.println("[Confirm logout, y/n: ]");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();
        if (answer.equals("y")) {
            bootstrap.getTokenEndpoint().closeTokenSession(token);
            bootstrap.setToken(null);
            System.out.println("[OK]");
        }
    }
}
