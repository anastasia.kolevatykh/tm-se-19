package ru.kolevatykh.spring.command.general;

import com.jcabi.manifests.Manifests;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show build number.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() {
        BasicConfigurator.configure();
        System.out.println("Version: " + Manifests.read("Version"));
        System.out.println("Build number: " + Manifests.read("BuildNumber"));
        System.out.println("Developer: " + Manifests.read("Developer"));
    }
}
