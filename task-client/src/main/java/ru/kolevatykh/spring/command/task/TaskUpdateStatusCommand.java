package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.TaskDTO;

public final class TaskUpdateStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected task status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println(Message.ID);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final TaskDTO taskDTO = bootstrap.getTaskEndpoint().findTaskById(token, id);
        if (taskDTO == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        System.out.println("Enter new STATUS name: PLANNED INPROCESS READY");
        @NotNull final String status = ConsoleInputUtil.getConsoleInput();
        if (status.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        bootstrap.getTaskEndpoint().mergeTaskStatus(token, id, status);
        System.out.println("[OK]");
    }
}
