package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.constant.Message;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        bootstrap.getTaskEndpoint().removeAllTasks(token);
        System.out.println("[" + getDescription() + "]");
        System.out.println(Message.OK);
    }
}
