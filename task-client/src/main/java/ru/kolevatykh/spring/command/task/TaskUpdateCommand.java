package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.endpoint.ProjectDTO;
import ru.kolevatykh.spring.endpoint.TaskDTO;

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter task id: ");
        @NotNull final String taskId = ConsoleInputUtil.getConsoleInput();
        if (taskId.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final TaskDTO taskDTO = bootstrap.getTaskEndpoint().findTaskById(token, taskId);
        if (taskDTO == null) {
            throw new Exception("[The task '" + taskId + "' does not exist!]");
        }

        System.out.println("Enter project id: ");
        @Nullable String projectId = ConsoleInputUtil.getConsoleInput();
        if (projectId.isEmpty() || projectId.equals("null")) {
            projectId = null;
            ;
        } else {
            @Nullable final ProjectDTO projectDTO = bootstrap.getProjectEndpoint().findProjectById(token, projectId);
            if (projectDTO == null) {
                throw new Exception("[The project '" + projectId + "' does not exist!]");
            }
        }

        System.out.println("Enter new task name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new task description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task start date: ");
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task end date: ");
        @NotNull final String finishDate = ConsoleInputUtil.getConsoleInput();

        bootstrap.getTaskEndpoint().mergeTask(token, projectId, taskId, name, description, startDate, finishDate);
        System.out.println("[OK]");
    }
}
