package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JacksonSaveJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-json-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "js";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save to json file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JACKSON JSON SAVE]");
        bootstrap.getDomainEndpoint().saveJacksonJson(token);
        System.out.println("[Saving is successful!]");
    }
}
