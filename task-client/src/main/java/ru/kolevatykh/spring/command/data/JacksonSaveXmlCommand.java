package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JacksonSaveXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xs";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JACKSON XML SAVE]");
        bootstrap.getDomainEndpoint().saveJacksonXml(token);
        System.out.println("[Saving is successful!]");
    }
}
