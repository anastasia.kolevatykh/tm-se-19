package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.TaskDTO;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println(Message.ID);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final TaskDTO taskDTO = bootstrap.getTaskEndpoint().findTaskById(token, id);
        if (taskDTO == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        bootstrap.getTaskEndpoint().removeTask(token, id);
        System.out.println(Message.OK);
    }
}
