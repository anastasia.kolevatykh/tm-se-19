package ru.kolevatykh.spring.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.endpoint.TaskDTO;
import ru.kolevatykh.spring.wrapper.TaskWrapper;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        @Nullable final List<TaskDTO> taskList = bootstrap.getTaskEndpoint().findAllTasksByUserId(token);
        if (taskList == null) {
            throw new Exception("[No tasks yet.]");
        }

        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;
        for (@NotNull final TaskDTO taskDTO : taskList) {
            @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
            tasks
                    .append(++i)
                    .append(". ")
                    .append(taskWrapper.toString())
                    .append(System.lineSeparator());
        }
        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
