package ru.kolevatykh.spring.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;

public final class JacksonLoadJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-json-Load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "jl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save to json file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[JACKSON XML LOAD]");
        bootstrap.getDomainEndpoint().loadJacksonJson(token);
        System.out.println("[Saving is successful!]");
    }
}
