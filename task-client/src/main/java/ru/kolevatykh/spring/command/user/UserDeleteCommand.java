package ru.kolevatykh.spring.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;

public final class UserDeleteCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-delete";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ud";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Delete account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("[Are you sure you want to delete your account? y/n]");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();

        if (answer.equals("y")) {
            System.out.println("Enter login to confirm removal: ");
            @NotNull final String login = ConsoleInputUtil.getConsoleInput();
            if (login.isEmpty()) {
                throw new Exception("[The name can't be empty.]");
            }

            @Nullable final String token = bootstrap.getToken();
            if (token == null) {
                throw new Exception("[The user '" + login + "' is not authorized!]");
            }

            bootstrap.getUserEndpoint().removeUser(token);
            bootstrap.setToken(null);
            System.out.println("[Deleted user account with projects and tasks.]");
            System.out.println(Message.OK);
        }
    }
}
