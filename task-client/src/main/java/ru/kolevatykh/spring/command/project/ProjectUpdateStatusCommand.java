package ru.kolevatykh.spring.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.util.ConsoleInputUtil;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.ProjectDTO;

public final class ProjectUpdateStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = bootstrap.getToken();
        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.ID);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final ProjectDTO projectDTO = bootstrap.getProjectEndpoint().findProjectById(token, id);
        if (projectDTO == null) {
            throw new Exception("[The project id '" + id + "' does not exist!]");
        }

        System.out.println(Message.STATUS);
        @NotNull final String status = ConsoleInputUtil.getConsoleInput();
        if (status.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        bootstrap.getProjectEndpoint().mergeProjectStatus(token, id, status);
        System.out.println(Message.OK);
    }
}
