package ru.kolevatykh.spring.constant;

import org.jetbrains.annotations.NotNull;

public final class WSDLConst {

    @NotNull public static final String TOKEN_URL = "http://0.0.0.0:1234/TokenEndpoint?wsdl";
    @NotNull public static final String DOMAIN_URL = "http://0.0.0.0:1234/DomainEndpoint?wsdl";
    @NotNull public static final String USER_URL = "http://0.0.0.0:1234/UserEndpoint?wsdl";
    @NotNull public static final String PROJECT_URL = "http://0.0.0.0:1234/ProjectEndpoint?wsdl";
    @NotNull public static final String TASK_URL = "http://0.0.0.0:1234/TaskEndpoint?wsdl";
}

