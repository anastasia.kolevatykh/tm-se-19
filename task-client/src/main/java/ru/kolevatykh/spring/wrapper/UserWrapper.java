package ru.kolevatykh.spring.wrapper;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.endpoint.UserDTO;

public class UserWrapper extends UserDTO {

    @NotNull
    private UserDTO user;

    public UserWrapper(@NotNull final UserDTO user) {
        this.user = user;
    }

    public UserWrapper() {
    }

    @Override
    public String toString() {
        return "id: '" + user.getId() + '\'' +
                ", login: '" + user.getLogin() + '\'' +
                ", roleType: " + user.getRoleType();
    }

    @NotNull
    public UserDTO getUser() {
        return this.user;
    }

    public void setUser(@NotNull final UserDTO user) {
        this.user = user;
    }
}
