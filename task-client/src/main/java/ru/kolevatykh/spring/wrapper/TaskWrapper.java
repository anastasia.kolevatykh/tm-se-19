package ru.kolevatykh.spring.wrapper;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.spring.endpoint.TaskDTO;

public class TaskWrapper extends TaskDTO {

    @NotNull
    private TaskDTO task;

    public TaskWrapper(@NotNull final TaskDTO taskDTO) {
        this.task = taskDTO;
    }

    public TaskWrapper() {
    }

    @Override
    public String toString() {
        return "userId: '" + task.getUserId() + '\'' +
                ", projectId: '" + task.getProjectId() + '\'' +
                ", id: '" + task.getId() + '\'' +
                ", name: '" + task.getName() + '\'' +
                ", description: '" + task.getDescription() + '\'' +
                ", statusType: " + task.getStatusType() +
                ", startDate: " + task.getStartDate() +
                ", finishDate: " + task.getFinishDate();
    }

    @NotNull
    public TaskDTO getTask() {
        return this.task;
    }

    public void setTask(@NotNull final TaskDTO task) {
        this.task = task;
    }
}
