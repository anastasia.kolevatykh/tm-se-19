package ru.kolevatykh.spring;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kolevatykh.spring.bootstrap.Bootstrap;
import ru.kolevatykh.spring.configuration.ApplicationConfig;

public final class ApplicationClient {
    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}
