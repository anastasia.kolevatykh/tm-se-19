package ru.kolevatykh.spring.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kolevatykh.spring.command.AbstractCommand;
import ru.kolevatykh.spring.constant.Message;
import ru.kolevatykh.spring.endpoint.*;
import ru.kolevatykh.spring.util.ConsoleInputUtil;

import java.lang.Exception;
import java.util.*;

@Component
public final class Bootstrap {

    @Nullable
    private String token;

    @NotNull
    public final ITaskEndpoint taskEndpoint;

    @NotNull
    public final IProjectEndpoint projectEndpoint;

    @NotNull
    public final IUserEndpoint userEndpoint;

    @NotNull
    public final IDomainEndpoint domainEndpoint;

    @NotNull
    public final ITokenEndpoint tokenEndpoint;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.kolevatykh.spring.command").getSubTypesOf(AbstractCommand.class);

    @Autowired
    public Bootstrap(@NotNull final ITaskEndpoint taskEndpoint,
                     @NotNull final IProjectEndpoint projectEndpoint,
                     @NotNull final IUserEndpoint userEndpoint,
                     @NotNull final IDomainEndpoint domainEndpoint,
                     @NotNull final ITokenEndpoint tokenEndpoint) {
        this.taskEndpoint = taskEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.userEndpoint = userEndpoint;
        this.domainEndpoint = domainEndpoint;
        this.tokenEndpoint = tokenEndpoint;
    }

    private void registryCommand(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String commandName = command.getName();
        @NotNull final String commandShortName = command.getShortName();
        @NotNull final String commandDescription = command.getDescription();

        if (commandName.isEmpty()) {
            throw new Exception("[There's no such command name.]");
        }
        if (commandDescription.isEmpty()) {
            throw new Exception("[There's no such command description.]");
        }
        command.setBootstrap(this);
        commands.put(commandName, command);

        if (!commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() {
        System.out.println(Message.WELCOME);
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleInputUtil.getConsoleInput();
            try {
                execute(command);
            } catch (Exception e) {
                if (e.getMessage().contains("[The access is forbidden.]")
                        || e.getMessage().contains("[The session is expired.]")
                        || e.getMessage().contains("[The session does not exist.]")
                        || e.getMessage().contains("The signature is invalid.")) {
                    setToken(null);
                    System.out.println("[Please authorize or register.]");
                }
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (token == null && abstractCommand.needAuth()) throw new Exception("[The access is forbidden.]");
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(new HashSet<>(commands.values()));
    }

    public void init() {
        try {
            for (@NotNull final Class<?> classItem : classes) {
                registryCommand((AbstractCommand) classItem.newInstance());
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public String getToken() {
        return this.token;
    }

    public void setToken(@Nullable final String token) {
        this.token = token;
    }

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @NotNull
    public ITokenEndpoint getTokenEndpoint() {
        return tokenEndpoint;
    }
}
